import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class Main extends Application {
    Stage window;

    public Main() {
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) throws Exception {
        this.window = primaryStage;
        AnchorPane layout = new AnchorPane();
        Scene scene = new Scene(layout, 400.0D, 400.0D);
        Circle circle = new Circle(200.0D, 200.0D, 80.0D, Color.GRAY);
        Circle leftEye = new Circle(180.0D, 180.0D, 10.0D, Color.WHITE);
        Circle rightEye = new Circle(210.0D, 180.0D, 10.0D, Color.WHITE);
        Circle mouth = new Circle(180.0D, 240.0D, 5.0D, Color.WHITE);
        Circle mouth1 = new Circle(165.0D, 230.0D, 5.0D, Color.WHITE);
        Circle mouth2 = new Circle(195.0D, 245.0D, 5.0D, Color.WHITE);
        Circle mouth3 = new Circle(210.0D, 245.0D, 5.0D, Color.WHITE);
        Circle mouth4 = new Circle(225.0D, 240.0D, 5.0D, Color.WHITE);
        Circle mouth5 = new Circle(235.0D, 230.0D, 5.0D, Color.WHITE);
        layout.getChildren().addAll(circle, leftEye, rightEye, mouth, mouth1, mouth2, mouth3, mouth4, mouth5);
        this.window.setScene(scene);
        this.window.setTitle("Picture");
        this.window.show();
    }
}